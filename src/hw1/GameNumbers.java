package hw1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class GameNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random random = new Random();
        int[] userNumbers = new int[100];

        System.out.println("Hallo! What is your name?");
        String name = scan.nextLine();

        int number = random.nextInt(100);
        int userNumber;

        System.out.println("Hi, " + name + "! Let the game begin! \nWe thought of a number from 0 to 100 - guess it");

        for (int i = 0;;i++) {
            System.out.println("Enter a number:");
            if(!scan.hasNextInt()) {
                System.out.println("The value is not a number! Lets try again.");
                continue;
            }

            userNumber = scan.nextInt();
            userNumbers[i] = userNumber;

            if (userNumber > number) {
                System.out.println("Your number is too big. Please, try again.");
            } else if (userNumber < number){
                System.out.println("Your number is too small. Please, try again.");
            } else {
                System.out.println("Congratulations, " + name + "!");
                sortArray(userNumbers);
                System.out.print("Your numbers: ");
                printArrayNElements(userNumbers, i+1);
                break;
            }
        }
    }

    private static void printArrayNElements (int[] inputArray, int quantity){
        int[] newArray = new int[quantity];
        for (int i = 0; i < quantity; i++) {
            newArray[i] = inputArray[i];
        }
        System.out.println(Arrays.toString(newArray));
    }
    private static void sortArray(int[] inputArray) {
        for(int i = 0; i < inputArray.length - i; i++) {
            for (int j = i; j < inputArray.length - i - 1; j++) {
                if (inputArray[j] < inputArray[j + 1]) {
                    int temp = inputArray[j];
                    inputArray[j] = inputArray[j + 1];
                    inputArray[j + 1] = temp;
                }
            }
            for (int j = inputArray.length -2 -i; j > i; j--) {
                if (inputArray[j] > inputArray[j - 1]) {
                    int temp = inputArray[j];
                    inputArray[j] = inputArray[j - 1];
                    inputArray[j - 1] = temp;
                }
            }
        }
    }
}
