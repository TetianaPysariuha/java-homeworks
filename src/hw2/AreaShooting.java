package hw2;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class AreaShooting {
    public static void main(String[] args) {
        char[][] field = new char[5][5];
        Scanner scan = new Scanner(System.in);
        Random random = new Random();

        int line = random.nextInt(4) + 1;
        int column = random.nextInt(4) + 1;

        for (int i = 0; i<field.length; i++){
            for (int j = 0; j <field[i].length; j++){
                field[i][j] = '-';
            }
        }

        int userLine;
        int userColumn;

        for (;;) {
            for(;;) {
                System.out.println("Enter a number of line from 1 to 5:");
                if(!scan.hasNextInt()) {
                    System.out.println("The value is not a number! Lets try again.");
                    scan.next();
                    continue;
                }
                userLine = scan.nextInt();
                if(userLine < 1 || userLine > 5) {
                    System.out.println("The value should be from 1 to 5! Lets try again.");
                    continue;
                }
                break;
            }

            for(;;) {
                System.out.println("Enter a number of column from 1 to 5:");
                if(!scan.hasNextInt()) {
                    System.out.println("The value is not a number! Lets try again.");
                    scan.next();
                    continue;
                }
                userColumn = scan.nextInt();
                if(userColumn < 1 || userColumn > 5) {
                    System.out.println("The value should be from 1 to 5! Lets try again.");
                    continue;
                }
                break;
            }

            if (line == userLine && column == userColumn) {
                System.out.println("You have won!");
                field[userLine-1][userColumn-1] = 'X';
                printField(field);
                break;
               } else {
                field[userLine-1][userColumn-1] = '*';
                printField(field);
            }
        }
    }

    private static void printField(char[][] inputArray){
        for(int i = 0; i < inputArray.length +1; i++) {
            System.out.print(i + "|");
        };
        System.out.println();
        for(int i = 0; i < inputArray.length; i++) {
            System.out.print((i + 1 ) + "|");
            for(int j = 0; j < inputArray[i].length; j++) {
                System.out.print(inputArray[i][j] + "|");
            }
            System.out.println();
        }
    }
}
