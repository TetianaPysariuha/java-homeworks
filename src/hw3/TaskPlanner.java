package hw3;

import java.util.Random;
import java.util.Scanner;

public class TaskPlanner {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[1][0] = "Monday";
        scedule[2][0] = "Tuesday";
        scedule[3][0] = "Wednesday";
        scedule[4][0] = "Thursday";
        scedule[5][0] = "Friday";
        scedule[6][0] = "Saturday";

        scedule[0][1] = "do home work";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][1] = "buy presents for family";
        scedule[3][1] = "cook a new dish";
        scedule[5][1] = "clean the house";
        scedule[6][1] = "have fun";

        for(;;){
            System.out.println("\nPlease, input the day of the week:");
            String week = new String(scan.nextLine());
            String[] command = week.split(" ");
            switch (command[0].toUpperCase().trim()){
                case "SUNDAY":
                    System.out.println(scedule[0][1]);
                    break;
                case "MONDAY":
                    System.out.println(scedule[1][1]);
                    break;
                case "TUESDAY":
                    System.out.println(scedule[2][1]);
                    break;
                case "WEDNESDAY":
                    System.out.println(scedule[3][1]);
                    break;
                case "THURSDAY":
                    System.out.println(scedule[4][1]);
                    break;
                case "FRIDAY":
                    System.out.println(scedule[5][1]);
                    break;
                case "SATURDAY":
                    System.out.println(scedule[6][1]);
                    break;
                case "CHANGE":
                    int index = -1;
                    if (command.length > 1) {
                        for(int i = 0; i < scedule.length; i++){
                            if ( scedule[i][0].equalsIgnoreCase(command[1]) ){
                                index = i;
                            }
                        }
                    }

                    if(index == -1) {
                        System.out.println("Day of week incorrect. Please, try again.");
                        break;
                    }
                    System.out.println("Please, enter new tasks");
                    scedule[index][1] = scan.nextLine();
                    break;
                case "EXIT":
                    return;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }
}
